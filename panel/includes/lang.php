<?php
   $lang = array();

   // admin.php
   $lang['Next'] = 'Next';
   $lang['Previous'] = 'Previous';

   // chart.php
   $lang['Count'] = 'amount';
   $lang['Hours'] = 'Clock';
   $lang['Days'] = 'Days';
   $lang['New password additions in the past month'] = 'Added passwords for the last month';
   $lang['New password additions in the past 24 hours'] = 'Added passwords in the last 24 hours';
   $lang['OS popularity'] = 'The popularity of operating systems';
   $lang['Additional OS statistics'] = 'Additional statistics on operating systems';

