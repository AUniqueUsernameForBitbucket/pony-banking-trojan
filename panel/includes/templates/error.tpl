<h4 class="achtung_header">
{if $err_code == 'ERR_NOT_ENOUGH_PRIVILEGES'}
	There was an error: Not enough privileges!
{elseif $err_code == 'ERR_UNK_LOG_ID'}
	An error occured: Invalid login ID!
{elseif $err_code == 'ERR_UNK_REPORT_ID'}
	An error occured: Invalid Report ID
{elseif $err_code == 'ERR_SRV_CONFIGURATION'}
	Attention! Problem with Server Configuration
{elseif $err_code == 'ERR_WRONG_PASSWORD'}
	An error occured: invalid password!
{elseif $err_code == 'ERR_PASSWORD_MISMATCH'}
	There was an error: The new password and confirmation do not match!
{elseif $err_code == 'ERR_EMPTY_PASSWORD'}
	An error occured: blank password!
{else}
	An error has occured
{/if}
</h4><br />

{if isset($back_link)}
<a href="{$smarty.server.SCRIPT_NAME}{$back_link nofilter}">Go back and try again</a>.
<br /><br />
{/if}